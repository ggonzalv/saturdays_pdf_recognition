# saturdays_pdf_recognition

This repository aims at reading an input pdf file, and extract all images and tables found on it. Then, it converts all detected tables into editable format ({.xlsx, .docx, .tex}, as specified by user). Additionally, it can convert a single table in jpg/png format to editable format.

# How to create docker image

To create your docker image, you first need to install docker on your compute. You can do this from the webpage:

https://www.docker.com

In addition, you might need to execute these commands to remove some trailing dependencies and be able to run the application:

```
rm -rf ~/Library/Group\ Containers/group.com.docker
rm -rf ~/Library/Containers/com.docker.docker
rm -rf ~/.docker
```

Now you are ready to create the docker image. You can build the image and run it typing the following commands:

```
docker build -f devops/dockerfiles/Dockerfile -t api-demo-saturday-ai:v1 .
docker run -p 5020:5020 api-demo-saturday-ai:v1
```

## How the code works

The logic within the code can be explained in the following steps:

1. First, the input pdf is split in different pages. All pages are converted into png images, and finally into cv2 format. These are stored in a temporary directory.
2. The transformers model is loaded. This model aims at detecting all images and tables within a jpg image. It returns the box coordinates of all tables and figures for each page.
3. The extracted coordinates are used to crop the images within the temporary directory.
4. For tables, OCR techniques are applied to detect all cells. The text is manipulated to preserve the original format.
5. The output images and tables are compressed, and the code returns a zip file as final output.

## References

- Interactive demo to test the transformers model with a document image: https://huggingface.co/spaces/nielsr/dit-document-layout-analysis
- Document Image Transformer main article: https://arxiv.org/abs/2203.02378

## Sponsors

https://www.bounsel.com/

![](Logo_color_positivo.png)

